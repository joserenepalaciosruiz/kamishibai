-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 07, 2016 at 04:00 PM
-- Server version: 10.0.17-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kamishibai`
--

-- --------------------------------------------------------

--
-- Table structure for table `tareas`
--

CREATE TABLE `tareas` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `completa` varchar(45) DEFAULT NULL,
  `horaDeTermino` varchar(45) DEFAULT NULL,
  `idContent` int(11) DEFAULT NULL,
  `idUsuario` int(11) DEFAULT NULL,
  `tipo` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tareas`
--

INSERT INTO `tareas` (`id`, `descripcion`, `completa`, `horaDeTermino`, `idContent`, `idUsuario`, `tipo`) VALUES
(0, 'tarea3', 'true', '12:40', 1, 1, 'Daily'),
(1, 'tarea1', 'true', '8:50', 1, 1, 'Daily'),
(2, 'tarea2', 'true', '12:15', 2, 1, 'Weekly'),
(3, 'tarea4', 'true', '8:50', 2, 2, 'Weekly'),
(4, 'tarea78', 'false', '9:90', 2, 1, 'Weekly'),
(5, 'tarea79', 'true', '6:40', 1, 2, 'Daily'),
(6, 'tarea37', 'true', '9:45', 3, 2, 'Monthly'),
(7, 'tarea91', 'false', '7:05', 3, 1, 'Monthly');

-- --------------------------------------------------------

--
-- Table structure for table `tipos`
--

CREATE TABLE `tipos` (
  `id` int(11) NOT NULL,
  `tipo` varchar(45) DEFAULT NULL,
  `idContent` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipos`
--

INSERT INTO `tipos` (`id`, `tipo`, `idContent`) VALUES
(1, 'Daily', '1'),
(2, 'Weekly', '2'),
(3, 'Monthly', '3');

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE `usuarios` (
  `idUsuario` int(11) NOT NULL,
  `nombreUsuario` varchar(45) DEFAULT NULL,
  `departamento` varchar(45) DEFAULT NULL,
  `idDepartamento` int(11) DEFAULT NULL,
  `idTareas` int(11) DEFAULT NULL,
  `admin` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`idUsuario`, `nombreUsuario`, `departamento`, `idDepartamento`, `idTareas`, `admin`) VALUES
(1, 'Rene Palacios', 'Quality', 7, 1, 'true'),
(2, 'Victor Gonzalez', 'Quality', 7, 2, 'true'),
(3, 'Sergio Palomo', 'Engineering', 8, 3, 'false');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tareas`
--
ALTER TABLE `tareas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipos`
--
ALTER TABLE `tipos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idUsuario`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

var app = angular.module('app', ['ngRoute', 'ngAnimate', 'toaster', 'ui.bootstrap']);

// configure our routes
app.config(function($routeProvider) {
  $routeProvider

    // route for the home page
    .when('/', {
      title: 'Dashboard',
      templateUrl : 'components/pages/home.html',
      controller  : 'authCtrl'
    })
    .when('/login', {
      title: 'Login',
      templateUrl: 'components/pages/login.html',
      controller: 'authCtrl'
    })
    .when('/signup', {
      title: 'Signup',
      templateUrl: 'components/pages/signup.html',
      controller: 'authCtrl'
    })
    .when('/logout', {
      title: 'Logout',
      templateUrl: 'components/pages/home.html',
      controller: 'logoutCtrl'
    })

    // route for the about page
    .when('/about', {
      templateUrl : 'components/pages/about.html',
      controller  : 'aboutController'
    })

    // route for the contact page
    .when('/contact', {
      templateUrl : 'components/pages/contact.html',
      controller  : 'contactController'
    });
});
app.run(function ($rootScope, $location, Data) {
    $rootScope.$on("$routeChangeStart", function (event, next, current) {
        $rootScope.authenticated = false;
        Data.get('session').then(function (results) {
            if (results.uid) {
                $rootScope.authenticated = true;
                $rootScope.uid = results.uid;
                $rootScope.name = results.name;
                $rootScope.email = results.email;
            } else {

            }
        });
    });
});

app.controller('mainController', function($scope) {
  $scope.message = 'Everyone come and see how good I look!';
});

app.controller('aboutController', function($scope) {
  $scope.message = 'Planeacion Stardard Work';
});

app.controller('contactController', function($scope) {
  $scope.message = 'Engineering Stardard Work';
});

app.controller('activeController', ['$scope','$location', function($scope, $location) {
    $scope.isActive = function(route) {
        return route === $location.path();
    }
}]);

app.controller('TasksController', ['$scope','$http','$filter', function($scope, $http, $filter){
      var store = this;
      store.products = [];
      getTask();

      $scope.cambiar = function(task) {
        if(task.completa === 'false') {
          getTask();
          return;
        }
        toggleStatus(task.id, task.completa);
      }
      $scope.cambiarFecha = function(task) {
        if(task.completa === 'false') {
          getTask();
          return;
        }
        toggleStatusDate(task.id, task.horaDeTermino, task.completa);
      }
      function getTask() {
        $http.get('http://127.0.0.1/departments/quality').success(function(data){
            for (var i = 0; i < data.length; i++) {
              if ($scope.uid === data[i].idUsuario) {
                data[i].isDisabled = false;
              }
            }
            store.products = data;
        });
      }
    function toggleStatus(item, status) {
      if(status=='true'){status='true';}else{status='false';}
          $http.put("http://127.0.0.1/tasks/"+item+"/"+status).success(function(data){
        });
      }
      function toggleStatusDate(item, date, status) {
        if(status=='false'){return;}
        function pad(n) {
          return (n < 10) ? '0' + n : n;
        }
        var time = new Date();
        var hours = time.getHours();
        var minutes = time.getMinutes();
        date = pad(hours) + ":" + pad(minutes);
        $http.put("http://127.0.0.1/tasks/put/"+item+"/"+date).success(function(data){
          getTask();
        });
      }
}]);

<?php
require_once 'dbHandler.php';
require_once 'passwordHash.php';
require_once 'dbConnect.php';
require 'vendor/autoload.php';

$app = new \Slim\Slim();
$app->config('debug', true);
$db = new dbConnect();
$conn = $db->connect();
$user_id = NULL;

require_once 'authentication.php';

$app->get('/hello/:name', function ($name) {
    echo "Hello, $name";
});

$app->get('/departments/:department', function ($department) use ($conn) {
  $sql = "Select idUsuario, nombreUsuario, departamento, idDepartamento, idTareas, admin from kamishibai.usuarios where departamento = '$department'";
  foreach ($conn->query($sql) as $row) {
    $data[] = array(
      "nombreUsuario" => $row['nombreUsuario'],
      "isDisabled" => true,
      "departamento" => $row['departamento'],
      "admin" => $row['admin'],
      "idDepartamento" => $row['idDepartamento'],
      "idUsuario" => $row['idUsuario'],
      "tareas" => getTareas($row['idUsuario'])
    );
  }
  echo json_encode($data);
});


function getTareas($idUsuario)
{
  global $conn;
  $sql = "SELECT idUsuario, descripcion, completa, horaDeTermino, tipo, idContent FROM kamishibai.tareas WHERE idUsuario = '$idUsuario' ORDER BY idContent";
  $iD=0; $iW=0; $iM=0;
  $data=null;
  foreach ($conn->query($sql) as $row) {
    if ($row['tipo'] == "Daily" && $iD==0) {
      $iD++;
      $data[] = array(
        'tipo' => "Daily",
        'content' => getContent($idUsuario, "Daily")
      );
    } elseif ($row['tipo'] == "Weekly"  && $iW==0) {
      $iW++;
      $data[] = array(
        'tipo' => "Weekly",
        'content' => getContent($idUsuario, "Weekly")

      );
    } elseif ($row['tipo'] == "Monthly"  && $iM==0) {
      $iM++;
      $data[] = array(
        'tipo' => "Monthly",
        'content' => getContent($idUsuario, "Monthly")
      );
    }
  }
  return $data;
}
function getContent($idUsuario, $tipo) {
  global $conn;
  $content = null;
  $sql = "SELECT  id, idUsuario, descripcion, completa, horaDeTermino, tipo, idContent FROM kamishibai.tareas WHERE idUsuario = '$idUsuario' and tipo = '$tipo' ORDER BY idContent";
  foreach ($conn->query($sql) as $row) {
    $content[] = array(
      "id" => $row['id'],
      "descripcion" => $row['descripcion'],
      "completa" => $row['completa'],
      "horaDeTermino" => $row['horaDeTermino']
    );
  }
  return $content;
}

$app->put('/tasks/:id/:completa', function ($id, $completa) use ($conn) {
  $sql = "UPDATE kamishibai.tareas SET completa='$completa' WHERE id = '$id'";
  $data = $conn->query($sql);
  echo json_encode($data);
});

$app->put('/tasks/put/:id/:date', function ($id, $date) use ($conn) {
  $sql = "UPDATE kamishibai.tareas SET horaDeTermino='$date' WHERE id = '$id'";
  $data = $conn->query($sql);
  echo json_encode($data);
});

function verifyRequiredParams($required_fields,$request_params) {
    $error = false;
    $error_fields = "";
    foreach ($required_fields as $field) {
        if (!isset($request_params->$field) || strlen(trim($request_params->$field)) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {

        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["status"] = "error";
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoResponse(200, $response);
        $app->stop();
    }
}


function echoResponse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    $app->status($status_code);
    $app->contentType('application/json');

    echo json_encode($response);
}
$app->run();
